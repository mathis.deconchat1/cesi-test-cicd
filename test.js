const assert = require('assert');
const { add, subtract, divide, multiply } = require("./operation");

describe('Test operations', () => {

    it('should add two numbers', () => {

        assert.equal(add(2, 3), 5);
    });

    it('should give an error', () => {
        assert.notEqual(add(2, 3), 10);
    });


    it('should subtract two numbers', () => {
        assert.equal(subtract(2, 3), -1);
    });

    it('should multiply two numbers', () => {
        assert.equal(multiply(2, 3), 6);
    });

    it('should divide two numbers', () => {
        assert.equal(divide(6, 2), 3);
    })
})

